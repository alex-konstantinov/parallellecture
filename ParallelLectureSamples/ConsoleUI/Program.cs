﻿using System;
using ClassLibrary;
using ClassLibrary.CancellationSamples;
using ClassLibrary.ParallelLibrary;
using ClassLibrary.PlinqSamples;
using ClassLibrary.ThreadSamples;

namespace ConsoleUI
{
    class Program
    {
        static void Main()
        {
            WelcomeToThreads.CreateSampleThread();
            //WelcomeToThreads.UsingJoinForThread();
            //WelcomeToThreads.UsingPriorityForThreads();

            //WelcomeToThreadPool.StartNewThreadUsingThreadPool();
            //WelcomeToThreadPool.SetNumberOfThreadsForThreadPool();

            //HowToCancelManagedThread.CancelThreadFromThreadPool();
            //HowToCancelManagedThread.CancelTask();

            //var wizard = new ComparisonWizard(new WelcomeToParallelInvoke.InvokeSequential(), new WelcomeToParallelInvoke.InvokeParallel());
            //wizard.CompareItems();

            //var wizard = new ComparisonWizard(new WelcomeToParallelForeach.ForeachSequential(), new WelcomeToParallelForeach.ForeachParallel());
            //wizard.CompareItems();

            //var wizard = new ComparisonWizard(new DoNotDoThisWithParallel.ForeachSequential(), new DoNotDoThisWithParallel.ForeachParallel());
            //wizard.CompareItems();

            //var wizard = new ComparisonWizard(new WelcomeToParallelFor.ForSequential(), new WelcomeToParallelFor.ForParallel());
            //wizard.CompareItems();

            //HowToUseParallelLoopStateAndToken.UsingParallelForeachWithLoopStateAndOptions();

            //HowToUseParallelLoopStateAndToken.UsingParallelForeachWithCancellationToken();

            //var wizard = new ComparisonWizard(new WelcomeToAsParallelMethod.LinqQuerySequential(), new WelcomeToAsParallelMethod.LinqQueryParallel());
            //wizard.CompareItems();

            //var wizard = new ComparisonWizard(new WelcomeToAsParallelMethod.LinqQueryParallelOrdered(), new WelcomeToAsParallelMethod.LinqQueryParallel());
            //wizard.CompareItems();

            //WelcomeToForAllMethod.ForAllExample();

            Console.WriteLine("Program finished. Press any key for exit.");
            Console.ReadKey();
        }
    }
}
