﻿using System;
using System.Globalization;
using System.Threading;

namespace ClassLibrary.ThreadSamples
{
    public static class WelcomeToThreadPool
    {
        public static void StartNewThreadUsingThreadPool()
        {
            ThreadPool.QueueUserWorkItem(ShowMessageFromPool, "enjoy");

            Console.WriteLine("Main thread does some work, then sleeps.");
            Thread.Sleep(1000);
        }

        public static void SetNumberOfThreadsForThreadPool()
        {
            const int numberOfThreads = 30;
            const int minNumberOfThreads = 28;

            var result = ThreadPool.SetMaxThreads(numberOfThreads, numberOfThreads);
            Console.WriteLine("Max number changed = {0}", result);

            result = ThreadPool.SetMinThreads(minNumberOfThreads, minNumberOfThreads);
            Console.WriteLine("Min number changed = {0}", result);


            for (var i = 0; i < 100; i++)
            {
                ThreadPool.QueueUserWorkItem(ShowPoolInfoAndDoWork, i.ToString(CultureInfo.InvariantCulture));
            }
        }

        static void ShowMessageFromPool(Object message)
        {
            while (true)
            {
                Console.WriteLine("Hello from thread pool - {0}", (string)message);
                Thread.Sleep(300);
            }
        }

        static void ShowPoolInfoAndDoWork(object threadNumber)
        {
            Console.WriteLine("{0} - Hello from thread pool.", threadNumber);
            Thread.Sleep(10000);
        }
    }
}
