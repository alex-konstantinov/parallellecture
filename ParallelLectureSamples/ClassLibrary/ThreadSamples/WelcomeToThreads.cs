﻿using System;
using System.Threading;

namespace ClassLibrary.ThreadSamples
{
    public static class WelcomeToThreads
    {
        public static void CreateSampleThread()
        {
            var runningProcess = new Process();
            var secondThread = new Thread(runningProcess.DoSomeWorkA) { IsBackground = true };

            secondThread.Start();

            Thread.Sleep(500);
            Console.WriteLine("Some work in main thread");
            Thread.Sleep(500);

            secondThread.Abort();

            Console.WriteLine("Process.DoSomeWorkA has finished");

            try
            {
                Console.WriteLine("Try to restart the Process.DoSomeWorkA thread");
                secondThread.Start();
            }
            catch (ThreadStateException)
            {
                Console.Write("ThreadStateException trying to restart Process.DoSomeWorkA. ");
                Console.WriteLine("Expected since aborted threads cannot be restarted.");
            }
        }

        public static void UsingJoinForThread()
        {
            var runningProcess = new Process();
            var secondThread = new Thread(runningProcess.DoSomeWorkA) { IsBackground = true };
            secondThread.Start();

            Thread.Sleep(500);

            secondThread.Join();

            Console.WriteLine("==========================");
            Console.WriteLine("Main process finished");
            Console.WriteLine("==========================");
        }

        public static void UsingPriorityForThreads()
        {
            var process = new Process();

            var threadOne = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadOne",
                Priority = ThreadPriority.Lowest
            };

            var threadTwo = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadTwo",
                Priority = ThreadPriority.Normal
            };

            var threadThree = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadThree",
                Priority = ThreadPriority.Normal
            };

            var threadFour = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadFour",
                Priority = ThreadPriority.Normal
            };

            var threadFive = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadFive",
                Priority = ThreadPriority.Highest
            };

            var threadSix = new Thread(process.DoWorkInLoop)
            {
                Name = "ThreadSix",
                Priority = ThreadPriority.Highest
            };


            threadOne.Start();
            threadTwo.Start();
            threadThree.Start();
            threadFour.Start();
            threadFive.Start();
            threadSix.Start();

            Thread.Sleep(10000);
            process.StopProcess();
        }
    }

    public class Process
    {
        public void DoSomeWorkA()
        {
            while (true)
            {
                Console.WriteLine("Process.DoSomeWorkA is running in background thread.");
                Thread.Sleep(300);
            }
        }

        bool loopSwitch = true;

        public void StopProcess()
        {
            this.loopSwitch = false;
        }

        public void DoWorkInLoop()
        {
            long count = 0;

            while (loopSwitch)
            {
                count++;
            }

            Console.WriteLine("{0} with {1,11} priority " + "has a count = {2,13}",
                Thread.CurrentThread.Name,
                Thread.CurrentThread.Priority,
                count.ToString("N0"));
        }
    }
}
