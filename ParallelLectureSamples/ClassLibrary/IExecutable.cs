﻿namespace ClassLibrary
{
    public interface IExecutable
    {
        void Execute();

        string Name { get; }
    }
}
