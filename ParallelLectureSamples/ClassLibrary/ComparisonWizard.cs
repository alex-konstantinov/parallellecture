﻿using System;
using System.Diagnostics;
using System.Threading;

namespace ClassLibrary
{
    public class ComparisonWizard
    {
        private readonly IExecutable firstItem;
        private readonly IExecutable secondItem;

        private readonly Stopwatch firstStopwatch = new Stopwatch();
        private readonly Stopwatch secondStopwatch = new Stopwatch();

        public ComparisonWizard(IExecutable firstItem, IExecutable secondItem)
        {
            this.firstItem = firstItem;
            this.secondItem = secondItem;
        }

        public void CompareItems()
        {
            var runFirstThread = new Thread(RunFirst);
            var runSecondThread = new Thread(RunSecond);

            runFirstThread.Start();
            runSecondThread.Start();

            runFirstThread.Join();
            runSecondThread.Join();
        }

        private void RunFirst()
        {
            this.firstStopwatch.Start();
            this.firstItem.Execute();
            this.secondStopwatch.Stop();
            Console.WriteLine("First item - '{0}'Executed successfuly. Elapsed time in seconds = {1}", this.firstItem.Name, this.firstStopwatch.Elapsed.TotalSeconds);
        }

        private void RunSecond()
        {
            this.secondStopwatch.Start();
            this.secondItem.Execute();
            this.secondStopwatch.Stop();
            Console.WriteLine("Second item item - '{0}'Executed successfuly. Elapsed time in seconds = {1}", this.secondItem.Name, this.secondStopwatch.Elapsed.TotalSeconds);
        }
    }
}
