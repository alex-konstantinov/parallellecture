﻿using System;
using System.Linq;
using System.Threading;

namespace ClassLibrary.PlinqSamples
{
    public class WelcomeToAsParallelMethod
    {
        public static int Count = 500;

        public static int DoSomeWork(int i)
        {
            Thread.Sleep(100);
            return i * i;
        }

        public class LinqQuerySequential : IExecutable
        {
            public void Execute()
            {
                var collection = Enumerable.Range(1, Count);

                var evenNums = collection.Where(c => c % 2 == 0).Select(DoSomeWork);
                Console.WriteLine("{0} even numbers out of {1} total",
                                  evenNums.Count(), collection.Count());
            }

            public string Name { get { return "Linq query sequential"; } }
        }

        public class LinqQueryParallel : IExecutable
        {
            public void Execute()
            {
                var collection = Enumerable.Range(1, Count);

                var evenNums = collection.AsParallel().Where(c => c % 2 == 0).Select(DoSomeWork);
                Console.WriteLine("{0} even numbers out of {1} total",
                                  evenNums.Count(), collection.Count());

            }

            public string Name { get { return "Linq query parallel"; } }
        }

        public class LinqQueryParallelOrdered : IExecutable
        {
            public void Execute()
            {
                var collection = Enumerable.Range(1, Count);

                var evenNums = collection.AsParallel().AsOrdered().Where(c => c % 2 == 0).Select(DoSomeWork);
                Console.WriteLine("{0} even numbers out of {1} total",
                                  evenNums.Count(), collection.Count());

            }

            public string Name { get { return "Linq query parallel as ordered"; } }
        }
    }
}
