﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace ClassLibrary.PlinqSamples
{
    public class WelcomeToForAllMethod
    {
        private static ConcurrentBag<int> concurrentBag = new ConcurrentBag<int>();

        public static void ForAllExample()
        {
            var nums = Enumerable.Range(10, 10000);
            var query = nums.AsParallel().Where(num => num % 1000 == 0);
            query.ForAll(e => concurrentBag.Add(DoSomeWork(e)));
        }

        private static int DoSomeWork(int source)
        {
            Thread.Sleep(200);
            Console.WriteLine(source * source);
            return source * source;
        }
    }
}
