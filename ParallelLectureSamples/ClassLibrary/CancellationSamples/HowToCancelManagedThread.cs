﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary.CancellationSamples
{
    public static class HowToCancelManagedThread
    {
        public static void CancelThreadFromThreadPool()
        {
            var cts = new CancellationTokenSource();

            ThreadPool.QueueUserWorkItem(DoSomeWork, cts.Token);

            Console.WriteLine("Press any key to cancel background operation");
            Console.ReadKey();

            cts.Cancel();
        }

        public static void CancelTask()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;

            Task.Factory.StartNew(
                () =>
                {
                    for (var i = 0; i < 100000; i++)
                    {
                        token.ThrowIfCancellationRequested();
                        Console.WriteLine("Result = {0}", i);
                        Thread.Sleep(300);
                    }
                }
                , token);

            Console.WriteLine("Press any key to cancel background task");
            Console.ReadKey();

            cts.Cancel();
        }

        private static void DoSomeWork(object obj)
        {
            var token = (CancellationToken)obj;
            for (var i = 0; i < 100000; i++)
            {
                Console.WriteLine("Result = {0}", i);
                Thread.Sleep(300);

                if (token.IsCancellationRequested)
                {
                    break;
                }
            }
        }
    }
}
