﻿using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary.ParallelLibrary
{
    public class WelcomeToParallelInvoke
    {
        public class InvokeSequential : IExecutable
        {
            public void Execute()
            {
                DoSomeWork_1();
                DoSomeWork_2();
                DoSomeWork_3();
                DoSomeWork_4();
            }

            public string Name
            {
                get { return "Invoke sequential"; }
            }
        }

        public class InvokeParallel : IExecutable
        {
            public void Execute()
            {
                Parallel.Invoke(DoSomeWork_1, DoSomeWork_2, DoSomeWork_3, DoSomeWork_4);
            }

            public string Name
            {
                get { return "Invoke parallel"; }
            }
        }

        public static void DoSomeWork_1()
        {
            Thread.Sleep(2000);
        }

        public static void DoSomeWork_2()
        {
            Thread.Sleep(2000);
        }

        public static void DoSomeWork_3()
        {
            Thread.Sleep(2000);
        }

        public static void DoSomeWork_4()
        {
            Thread.Sleep(3000);
        }
    }
}
