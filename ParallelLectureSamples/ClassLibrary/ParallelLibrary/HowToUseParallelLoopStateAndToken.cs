﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary.ParallelLibrary
{
    public static class HowToUseParallelLoopStateAndToken
    {
        public static void UsingParallelForeachWithLoopStateAndOptions()
        {
            var collection = GetSamples();
            var loopResult = Parallel.ForEach(
                collection,
                new ParallelOptions { MaxDegreeOfParallelism = 10 },
                (item, state) =>
                {
                    if (state.ShouldExitCurrentIteration)
                    {
                        Console.WriteLine("Loop stopped. Current iteration = {0}", item);
                    }

                    if (item == 10000)
                    {
                        state.Break();
                    }
                });

            Console.WriteLine("Loop is completed? ...... {0}", loopResult.IsCompleted);
            Console.WriteLine("Lowest brear iteration = {0}", loopResult.LowestBreakIteration);
        }

        public static void UsingParallelForeachWithCancellationToken()
        {
            var nums = GetSamples();
            var cts = new CancellationTokenSource();

            var options = new ParallelOptions
            {
                CancellationToken = cts.Token,
                MaxDegreeOfParallelism = Environment.ProcessorCount
            };

            Console.WriteLine("Press any key to start. Press 'c' to cancel.");
            Console.ReadKey();

            ThreadPool.QueueUserWorkItem(delegate
            {
                if (Console.ReadKey().KeyChar == 'c')
                    cts.Cancel();
            });

            try
            {
                Parallel.ForEach(nums, options, iteration =>
                {
                    Thread.Sleep(500);
                    Console.WriteLine("Current iteration - {0}", iteration);
                    options.CancellationToken.ThrowIfCancellationRequested();
                });
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Operation was canceled");
            }
        }

        public static IEnumerable<int> GetSamples()
        {
            for (var i = 0; i < 100000; i++)
            {
                yield return i;
            }
        }
    }
}
