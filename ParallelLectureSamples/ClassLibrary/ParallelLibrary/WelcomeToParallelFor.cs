﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary.ParallelLibrary
{
    public class WelcomeToParallelFor
    {
        public class ForSequential : IExecutable
        {
            private readonly List<Action<int>> collection = GetActions().ToList();

            public void Execute()
            {
                for (var i = 0; i < this.collection.Count(); i++)
                {
                    this.collection[i](i);
                }
            }

            public string Name { get { return "For sequential"; } }
        }

        public class ForParallel : IExecutable
        {
            private readonly List<Action<int>> collection = GetActions().ToList();

            public void Execute()
            {
                Parallel.For(0, this.collection.Count, counter => this.collection[counter](counter));
            }

            public string Name { get { return "For parallel"; } }
        }

        public static IEnumerable<Action<int>> GetActions()
        {
            for (var i = 0; i < 10; i++)
            {
                yield return (count) => Thread.Sleep(1000 * count);
            }
        }
    }
}
