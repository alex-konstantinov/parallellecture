﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ClassLibrary.ParallelLibrary
{
    public class DoNotDoThisWithParallel
    {
        public class ForeachSequential : IExecutable
        {
            private readonly IEnumerable<Action> collection = GetActions(); 

            public void Execute()
            {
                foreach (var action in this.collection)
                {
                    action();
                }
            }

            public string Name { get { return "Foreach sequential"; } }
        }

        public class ForeachParallel : IExecutable
        {
            private readonly IEnumerable<Action> collection = GetActions(); 

            public void Execute()
            {
                Parallel.ForEach(this.collection, action => action());
            }

            public string Name { get { return "Foreach parallel"; } }
        }

        public static IEnumerable<Action> GetActions()
        {
            for (var i = 0; i < 1000000000; i++)
            {
                yield return () => {
                    // just do nothing
                };
            }

        }
    }
}
